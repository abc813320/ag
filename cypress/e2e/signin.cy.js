describe('Login to Ambition Guru with Mobile Number and OTP', () => {
    it('Should login with mobile number and OTP', () => {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number and submit
        cy.get('.q-form > :nth-child(1)').type('9819384203'); // Enter mobile number
        cy.get('.q-btn__content').click(); // Click submit button

        // Wait for OTP input field to appear and enter OTP
        //cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('452280'); // Enter OTP

        // Click verify button
        cy.get('.q-btn').click()
        
        // Assert navigation or any other actions after successful login
    });
    it('Shouldn/t login with mobile number and  invalid OTP', () => {
        cy.visit('https://app.ambition.guru/login');
        cy.get('.q-form > :nth-child(1)').type('9827358423'); 
        cy.get('.q-btn__content').click(); // Click submit button
        cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('12334'); // Enter OTP
        cy.get('.q-btn').click()
        cy.get('.tw-rounded')
    })

it('Shouldn/t login with  invalid mobile number', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type('123456789'); 
    cy.get('.q-btn__content').click(); 

    cy.get('.q-field__messages > div').should('be.visible');
})
it('Shouldn/t login with empty mobile number', () => {
    cy.visit('https://app.ambition.guru/login');
       // cy.get('.q-form > :nth-child(1)').type(''); 
        cy.get('.q-btn__content').click();
        cy.get('.q-field__messages > div').should('be.visible');

})
it('Shouldn/t login with special character mobile number', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type('98273584@#');
    cy.get('.q-btn__content').click();

    cy.get('.q-field__messages > div').should('be.visible');
})
it('Shouldn/t login with  8 digit mobile number', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type('98273584');
    cy.get('.q-btn__content').click();

    cy.get('.q-field__messages > div').should('be.visible');
})
it('Shouldn/t login withunregistered mobile number', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type('9842031832');
    cy.get('.q-btn__content').click();

    cy.get('.q-field__messages > div').should('be.visible');
})
it('Shouldn/t login with the alphabetical characters', () => {
    cy.visit('https://app.ambition.guru/login');
    cy.get('.q-form > :nth-child(1)').type('abcdeef');
    cy.get('.q-btn__content').click();

    cy.get('.q-field__messages > div').should('be.visible');
})







});
