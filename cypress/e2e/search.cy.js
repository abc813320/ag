import LoginPage from '../support/pageObjects/LoginPage';
import SearchPage from '../support/pageObjects/searchpage';

describe('Search Functionality Tests', () => {
    const loginPage = new LoginPage();
    const searchPage = new SearchPage();

    // Fixture data for login and search scenarios
    const loginData = require('../fixtures/loginData.json');
    const searchData = require('../fixtures/search.json');

    beforeEach(() => {
        // Log in before each test using a valid mobile number and auto-validated OTP
        loginPage.login(loginData.validMobileNumber, null); // Assuming auto-validation or OTP is not required
    });

    it('should search for a valid package and select a result', () => {
        const searchTerm = searchData.validSearchTerm; // Use search term from fixture data
        searchPage.performSearch(searchTerm);
        searchPage.selectSearchResult();

        // Assertions to verify the search results
       // cy.url().should('include', '/search-results'); // Update based on actual URL structure
       // cy.get('.result-title').should('contain.text', searchTerm); // Update with the correct selector and text
    });
    it('should handle search with an invalid search term', () => {
        const searchTerm = searchData.invalidSearchTerm; // Use invalid search term from fixture data
        searchPage.performSearch(searchTerm);
    
        // Assertions to verify no results message
        //cy.get('.no-results-message').should('be.visible').and('contain.text', 'No results found'); // Update with actual selector and message
    });
    it('should handle search with special characters in the search term', () => {
        const searchTerm = searchData.specialCharactersSearchTerm; // Use search term with special characters from fixture data
        searchPage.performSearch(searchTerm);
    
        // Assertions to verify search functionality with special characters
       //cy.url().should('not.include', '/search-results'); // Verify that search results page is not displayed
        
    });
    

    it('should search and sort results by relevance', () => {
        const searchTerm = searchData.validSearchTerm; // Use valid search term from fixture data
        searchPage.performSearch(searchTerm);
        searchPage.sortByRelevance();
    
        // Assertions to verify sorted results
      //  cy.url().should('include', '/search-results'); // Update based on actual URL structure
        // Add assertions to verify the sorted order of results by relevance
    });
})    
