describe('OTP Authentication', () => {
    it('should successfully redirect to videos', () => {
        // Visit the login page
    
        cy.visit('https://app.ambition.guru/login');
        cy.get('.q-form > :nth-child(1)').type (9819384203)
        cy.get('.q-btn__content').click()
        //cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('546957'); // Enter OTP

        // Click verify button
        cy.get('.q-btn').click()
        

         cy.get(':nth-child(6) > .q-item__section--side > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
         cy.get(':nth-child(6) > .q-item__section--main').click()
         cy.get('.q-tab--active > .q-tab__content > .q-tab__label').should('be.visible')
        //successful login
    })
    it('should successfully play the videos', () => {
        // Visit the login page
    
        cy.visit('https://app.ambition.guru/login');
        cy.get('.q-form > :nth-child(1)').type (9819384203)
        cy.get('.q-btn__content').click()
        //cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('546957'); // Enter OTP

        // Click verify button
        cy.get('.q-btn').click()
        

         cy.get(':nth-child(6) > .q-item__section--side > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
         cy.get(':nth-child(6) > .q-item__section--main').click()
         cy.get('.q-tab--active > .q-tab__content > .q-tab__label').should('be.visible')
         cy.get(':nth-child(1) > .carousel > .carousel__viewport > .carousel__track > .carousel__slide--active > .p-10 > .image > .relative-position > .q-img > .q-img__container > .q-img__image').click()
        //successful login
    })
    it('should successfully pause the videos', () => {
        // Visit the login page
    
        cy.visit('https://app.ambition.guru/login');
        cy.get('.q-form > :nth-child(1)').type (9819384203)
        cy.get('.q-btn__content').click()
        //cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('546957'); // Enter OTP

        // Click verify button
        cy.get('.q-btn').click()
        

         cy.get(':nth-child(6) > .q-item__section--side > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
         cy.get(':nth-child(6) > .q-item__section--main').click()
         cy.get('.q-tab--active > .q-tab__content > .q-tab__label').should('be.visible')
         cy.get(':nth-child(1) > .carousel > .carousel__viewport > .carousel__track > .carousel__slide--active > .p-10 > .image > .relative-position > .q-img > .q-img__container > .q-img__image').click()
         cy.get('.vjs-fullscreen-control > .vjs-icon-placeholder').click()
    })
})