describe('Login to Ambition Guru with Mobile Number and OTP', () => {
    it('Should login with mobile number and OTP and redirect to the audio section', () => {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number and submit
        cy.get('.q-form > :nth-child(1)').type('9819384203'); // Enter mobile number
        cy.get('.q-btn__content').click(); // Click submit button

        // Wait for OTP input field to appear and enter OTP
       // cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('279312'); // Enter OTP

        // Click verify button
        //cy.get('.q-btn').click()
        cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click()
        
    })
    it('Should play the audio', () => {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number and submit
        cy.get('.q-form > :nth-child(1)').type('9819384203'); // Enter mobile number
        cy.get('.q-btn__content').click(); // Click submit button

        // Wait for OTP input field to appear and enter OTP
       // cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('279312'); // Enter OTP

        // Click verify button
        //cy.get('.q-btn').click()
        cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click()
        cy.get('.relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
    })
    it('Should forward the video by 10 sec', () => {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number and submit
        cy.get('.q-form > :nth-child(1)').type('9819384203'); // Enter mobile number
        cy.get('.q-btn__content').click(); // Click submit button

        // Wait for OTP input field to appear and enter OTP
       // cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('279312'); // Enter OTP

        // Click verify button
        //cy.get('.q-btn').click()
        cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click()
        cy.get('.relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
        cy.get(':nth-child(4) > .q-btn__content > .q-img > .q-img__loading > .q-spinner').click()
    })
    it('Should display the comment section', () => {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number and submit
        cy.get('.q-form > :nth-child(1)').type('9819384203'); // Enter mobile number
        cy.get('.q-btn__content').click(); // Click submit button

        // Wait for OTP input field to appear and enter OTP
       // cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('279312'); // Enter OTP

        // Click verify button
        //cy.get('.q-btn').click()
        cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click()
        cy.get('.relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
        cy.get('.delay-200 > .d-flex > .q-img > .q-img__container > .q-img__image').click()
    })
    it('Should submit  the comment ', () => {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number and submit
        cy.get('.q-form > :nth-child(1)').type('9819384203'); // Enter mobile number
        cy.get('.q-btn__content').click(); // Click submit button

        // Wait for OTP input field to appear and enter OTP
       // cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('279312'); // Enter OTP

        // Click verify button
        //cy.get('.q-btn').click()
        cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click()
        cy.get('.relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click()
        cy.get('.delay-200 > .d-flex > .q-img > .q-img__container > .q-img__image').click()
        cy.get('#q-portal--dialog--2 > .q-dialog > .q-dialog__inner > .q-card > :nth-child(1) > .modal-body > .q-form > .q-col-gutter-md > :nth-child(1) > .q-field > .q-field__inner > .q-field__control').type('nice ')

        cy.get('#q-portal--dialog--2 > .q-dialog > .q-dialog__inner > .q-card > :nth-child(1) > .modal-body > .q-form > .footer > .q-btn--standard').click()
   
    })
    
    

})