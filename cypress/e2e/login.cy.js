import LoginPage from '../support/pageObjects/LoginPage';

describe('Login to Ambition Guru with Mobile Number and OTP', () => {
    //const loginPage = new LoginPage();

    beforeEach(() => {
        loginPage.visitLoginPage();
    });

    it('Should login with mobile number and OTP', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.validMobileNumber);
            loginPage.clickSubmitButton();
            loginPage.enterOTP(data.validOTP);
            loginPage.clickVerifyButton();
            // Assert navigation or any other actions after successful login
        });
    });

    it('Shouldn\'t login with mobile number and invalid OTP', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.ValidMobileNumber);
            loginPage.clickSubmitButton();
            loginPage.enterOTP(data.invalidOTP);
            loginPage.clickVerifyButton();
            // Assert error message or any other actions when login fails
        });
    });

    it('Should display error for invalid mobile number format', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.invalidMobileNumber);
            loginPage.clickSubmitButton();
            // Assert error message or any other actions for invalid mobile number format
        });
    });

    it('Should display error for empty mobile number', () => {
        loginPage.clickSubmitButton(); // Don't enter mobile number
        // Assert error message or any other actions for empty mobile number
    });

    it('Should display error for special character mobile number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.specialcharacters);
            loginPage.clickSubmitButton();
            // Assert error message or any other actions for special character mobile number
        });
    });

    it('Should display error for 8-digit mobile number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.eightDigitMobileNumber);
            loginPage.clickSubmitButton();
            // Assert error message or any other actions for 8-digit mobile number
        });
    });

    it('Should display error for unregistered mobile number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.unregisteredMobileNumber);
            loginPage.clickSubmitButton();
            // Assert error message or any other actions for unregistered mobile number
        });
    });

    it('Should display error for alphabetical characters in mobile number', () => {
        cy.fixture('loginData').then((data) => {
            loginPage.enterMobileNumber(data.alphabeticalMobileNumber);
            loginPage.clickSubmitButton();
            // Assert error message or any other actions for alphabetical characters in mobile number
        });
    });

    // Additional test cases for mobile number input validation
});
