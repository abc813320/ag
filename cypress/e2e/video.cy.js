import LoginPage from '../support/pageObjects/LoginPage';
import VideoPage from '../support/pageObjects/VideoPage';

describe('Video Section Tests after Login', () => {
    const loginPage = new LoginPage();
    const videoPage = new VideoPage();
    const mobileNumber = '9819384203';
    // const otp = '279312'; // Uncomment and update with the actual OTP if needed

    beforeEach(() => {
        loginPage.login(mobileNumber, null); // Assuming OTP is not needed
    });

    it('should successfully redirect to videos', () => {
        videoPage.navigateToVideoSection();
        videoPage.clickOnCourse();
        videoPage.verifyVideoPage();
    });

    it('should successfully play videos', () => {
        videoPage.navigateToVideoSection();
        videoPage.clickOnCourse();
        videoPage.verifyVideoPage();
        videoPage.clickOnVideo();
    });

    it('should successfully make video interactive', () => {
        videoPage.navigateToVideoSection();
        videoPage.clickOnCourse();
        videoPage.clickOnVideo();
        videoPage.toggleVideo();
    });

    it('should successfully redirect to ask section', () => {
        videoPage.navigateToVideoSection();
        videoPage.clickOnCourse();
        videoPage.clickOnVideo();
        videoPage.goToAskSection();
    });
});
