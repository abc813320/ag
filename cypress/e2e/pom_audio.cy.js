import AudioPage from '../support/pageObjects/AudioPage';

describe('Audio Section Tests after Login', () => {
    const audioPage = new AudioPage();
    const mobileNumber = '9819384203';
    // const otp = '279312'; // Uncomment and update with the actual OTP if needed

    beforeEach(() => {
        // Assume OTP is not required or is auto-validated for the purpose of this example
        audioPage.loginAndNavigateToAudio(mobileNumber, null);
    });

    it('Should play the audio', () => {
        audioPage.playAudio();
    });

    it('Should forward the audio by 10 sec', () => {
        audioPage.playAudio();
        audioPage.forwardAudio();
    });

    it('Should display the comment section', () => {
        audioPage.playAudio();
        audioPage.openCommentSection();
    });

    it('Should submit the comment', () => {
        audioPage.playAudio();
        audioPage.openCommentSection();
        audioPage.submitComment('nice');
    });
});
