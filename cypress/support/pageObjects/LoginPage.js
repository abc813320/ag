class LoginPage {
    visit() {
        cy.visit('https://app.ambition.guru/login');
    }

    enterMobileNumber(mobileNumber) {
        cy.get('.q-form > :nth-child(1)').type(mobileNumber);
    }

    submitMobileNumber() {
        cy.get('.q-btn__content').click();
    }

    enterOTP(otp) {
        cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type(otp);
    }

    verifyOTP() {
        cy.get('.q-btn').click();
    }

    login(mobileNumber, otp) {
        this.visit();
        this.enterMobileNumber(mobileNumber);
        this.submitMobileNumber();
        if (otp) {
            this.enterOTP(otp);
            this.verifyOTP();
        }
    }
}

export default LoginPage;
