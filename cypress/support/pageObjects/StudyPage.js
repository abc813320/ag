class StudyPage {
    login(mobileNumber, otp) {
        // Visit the login page
        cy.visit('https://app.ambition.guru/login');

        // Enter mobile number
        cy.get('.q-form > :nth-child(1)').type(mobileNumber);
        cy.get('.q-btn__content').click();

        // Enter OTP
        cy.get(':nth-child(2) > .q-field > .q-field__inner > .q-field__control').type(otp);
        //cy.get('.q-btn').click();
    }

    visitStudySection() {
        // Navigate to the study section
        cy.visit('');
    }

    navigateToSubjectCategories() {
        // Click on the link/button to navigate to subject categories
        cy.get('.study-nav').click();
    }

    selectSubjectCategory(categoryName) {
        // Click on a specific subject category from the list
        cy.contains('.subject-category', categoryName).click();
    }

    browseStudyMaterials() {
        // Explore the available study materials, resources, or courses listed within the selected subject category
        cy.get('.study-materials').find('.study-material').each((material) => { /* Perform actions */ });
    }

    viewLiveClass() {
        // Click on the link/button to view the live class
        cy.get('.live-class').click();
    }

    viewVideos() {
        // Click on the link/button to view videos
        cy.get('.videos').click();
    }

    viewNotes() {
        // Click on the link/button to view notes
        cy.get('.notes').click();
    }

    // Add more methods as needed for interacting with different elements on the study page
}

export default StudyPage;
