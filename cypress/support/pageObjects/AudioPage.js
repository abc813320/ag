import LoginPage from './LoginPage';

class AudioPage {
    constructor() {
        this.loginPage = new LoginPage();
    }

    loginAndNavigateToAudio(mobileNumber, otp) {
        this.loginPage.login(mobileNumber, otp);
        cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click();
    }

    playAudio() {
        cy.get('.relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click();
    }

    forwardAudio() {
        cy.get(':nth-child(4) > .q-btn__content > .q-img > .q-img__loading > .q-spinner').click();
    }

    openCommentSection() {
        cy.get('.delay-200 > .d-flex > .q-img > .q-img__container > .q-img__image').click();
    }

    submitComment(comment) {
        cy.get('#q-portal--dialog--2 > .q-dialog > .q-dialog__inner > .q-card > :nth-child(1) > .modal-body > .q-form > .q-col-gutter-md > :nth-child(1) > .q-field > .q-field__inner > .q-field__control').type(comment);
        cy.get('#q-portal--dialog--2 > .q-dialog > .q-dialog__inner > .q-card > :nth-child(1) > .modal-body > .q-form > .footer > .q-btn--standard').click();
    }
}

export default AudioPage;
