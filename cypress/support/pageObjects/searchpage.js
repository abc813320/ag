class SearchPage {
    performSearch(searchTerm) {
        // Use backticks for template literals and fix selector syntax
        cy.get('.flex > .q-field > .q-field__inner > .q-field__control')
          .type(`${searchTerm}{enter}`);
    }

    selectSearchResult() {
        // Update selector based on common image element patterns
        cy.get('.q-field__control-container > .d-flex > .q-img > .q-img__container > .q-img__image')
          .click();
    }
}

export default SearchPage;
