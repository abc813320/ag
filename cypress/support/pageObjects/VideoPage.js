class VideoPage {
  navigateToVideoSection() {
      cy.get(':nth-child(3) > .q-tab__content > .q-tab__label').click();
  }

  clickOnCourse() {
      cy.get('.relative-position > :nth-child(1) > .q-img > .q-img__container > .q-img__image').click();
  }

  verifyVideoPage() {
      cy.url().should('include', '/video');
  }

  clickOnVideo() {
      cy.get('.video-item').first().click(); // Update the selector based on actual video items
  }

  toggleVideo() {
      cy.get('.video-controls .play-pause-button').click(); // Update with the actual selector
  }

  goToAskSection() {
      cy.get('.ask-section-link').click(); // Update with the actual selector
  }
}

export default VideoPage;
